# BlackAndWhite-GTK

This is a simple black and white theme for minimalists and people who are still rocking B/W monitors.

## Getting started

1. Clone the repository with `git clone https://gitlab.com/hellfire103/blackandwhite-gtk.git` or download the zip archive
2. Move the files to either `~/.themes`, `~/.local/share/themes`, or `/usr/share/themes`

## Screenshots

*Tested on Debian 11 with Xfce. Icon theme not included.*

![](https://i.ibb.co/Y7vZLW7/Black-And-White.png)

![](https://i.ibb.co/xLQmqWV/Black-And-White2.png)
